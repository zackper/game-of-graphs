# Game of Graphs

## Description
Face of in a 1v1 battle of wits in a game of graphs. A simple Go like strategy game implemented in Unity.

### Rules of the game:
Each player on their first turn only, can select any unclaiemd node. For the rest of the game until there are no more available moves, each player selects one node that is directly connected to at least one of their claimed nodes.

## Play the game on your browser here:
https://zackper.itch.io/game-of-graphs

## Documentation
Complete [report](https://docs.google.com/document/d/1TI8WurFWm6SxGksc8UetQ1xH4GEjjlOW7Qh8AhCZ-Ic/edit?usp=sharing) regarding the implementation of the game:

Power point [presentation](https://docs.google.com/presentation/d/1khFrZLrPDB_SRy86qJsnm3bTgBe-KPzAEePLVp7_88c/edit?usp=sharing)

## Give credit where credit is due:
[Professor Ioannis G. Tollis](https://www.csd.uoc.gr/CSD/index.jsp?custom=ioannis_tollis&lang=en) for proposing this project idea and allowing me to design and implement it.