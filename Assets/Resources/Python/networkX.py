import networkx as nx;
import matplotlib.pyplot as plt
import json;
import random;


graphSize = 3;

def DrawGraph(G):
    # subax1 = plt.subplot()
    nx.draw(G, with_labels=True, font_weight='bold')
    plt.show();
    None;

def WriteAsGoGFile(G):
    print(list(G.nodes))
    print(list(G.edges));
    G.number_of_nodes();
    G.number_of_edges();
    
    graph = {};
    # Set sizes
    graph["sizeX"] = graphSize;
    graph["sizeY"] = graphSize;
    # Set edges {x: number, y: number}
    edgesArray = [];
    for edge in list(G.edges):
        edgesArray.append({"x": edge[0], "y": edge[1]});
    graph["edges"] = edgesArray;
    # Set shortcut edges

    # Set point nodes {x: number, y: number}
    pointNodeArray = [];
    for i in range(2):
        randint = random.randint(0, graphSize * graphSize -1 );
        while(randint in pointNodeArray):
            randint = random.randint(0, graphSize * graphSize -1 );
        pointNodeArray.append(randint);
    graph["pointNodes"] = pointNodeArray;

    with open('gog.json', 'w') as outfile:
        json.dump(graph, outfile)

G = nx.Graph();
G = nx.connected_watts_strogatz_graph(pow(graphSize, 2), 4, 0)

WriteAsGoGFile(G);
DrawGraph(G);