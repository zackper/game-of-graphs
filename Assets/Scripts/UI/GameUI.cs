using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.Netcode;
using TMPro;

public class GameUI : NetworkBehaviour
{
    // While playing UI
    public TextMeshProUGUI currentPlayer;
    public TextMeshProUGUI currentInstance;
    public TextMeshProUGUI redPlayerScore;
    public TextMeshProUGUI bluePlayerScore;
    // Game ended
    public GameObject       gameEnded;
    [SerializeField]
    private TextMeshProUGUI winner = null;
    // New Graph UI
    public TMP_InputField graphSizeInput;
    public TMP_InputField shortcutNumberInput;
    public TMP_InputField pointNodesInput;
    // Message
    public GameObject messagePopupContainer;
    public TextMeshProUGUI messagePopup;


    private void Start()
    {
        gameEnded.SetActive(false);
    }

    public void SetCurrentPlayer(string text, Color color)
    {
        currentPlayer.text = text;
        currentPlayer.color = color;
        UpdateCurrentInstance();
    }
    public void UpdateCurrentInstance()
    {
        if (Config.Instance.versus == Config.Versus.PLAYER_PC)
            currentInstance.text = "";
        else
        {
            if (GameManager.Instance.IsLocalInstanceTurn())
                currentInstance.text = "(YOU)";
            else
                currentInstance.text = "(OPPONENT)";
        }
    }

    public void SetPlayerPoints(int points, Color color)
    {
        if (color == Color.red)
            redPlayerScore.text = points.ToString();
        else if (color == Color.blue)
            bluePlayerScore.text = points.ToString();
    }

    public void EndGameScreen(string name, Color color)
    {
        gameEnded.SetActive(true);
        winner.text = name;
        winner.color = color;
    }
    public void OnPlayAgainClick()
    {
        if (!NetworkManager.IsConnectedClient || IsHost)
        {
            gameEnded.SetActive(false);
            GameManager.Instance.StartGame();
        }
        else
            MessagePopup("Only the host can start the game.", 2);
    }
    public void OnGameStart()
    {
        gameEnded.SetActive(false);
    }

    public void PlayBestMove()
    {
        if (Config.Instance.graphSize <= 4)
            GameManager.Instance.PlayBestMove();
        else
            MessagePopup("AI has been disabled for graphs larger than size of 4.", 3);
    }

    public void MessagePopup(string message, int seconds)
    {
        StartCoroutine(MessagePopupRoutine(message, seconds));
    }
    private IEnumerator MessagePopupRoutine(string message, int seconds)
    {
        messagePopup.text = message;
        messagePopupContainer.SetActive(true);
        yield return new WaitForSeconds(seconds);
        messagePopupContainer.SetActive(false);
    }

    public void Quit()
    {
        GameManager.Instance.ResetBoard();
        NetworkManager.Shutdown();
        GetComponent<MainMenu>().mainMenu.SetActive(true);
        GetComponent<MainMenu>().SwitchMenu("Player versus");
    }
    public void NewGraph()
    {
        if (!NetworkManager.IsConnectedClient || IsHost)
        {
            // Set values to config
            Config.Instance.graphSize = int.Parse(graphSizeInput.text);
            Config.Instance.shortcuts = int.Parse(shortcutNumberInput.text);
            Config.Instance.pointNodes = int.Parse(pointNodesInput.text);
            // Call for new game
            GameManager.Instance.StartGame();
        }
        else
            MessagePopup("Only host can create new graph!", 3);
    }
}
