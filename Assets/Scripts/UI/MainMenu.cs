using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UNET;
using UnityEngine.SceneManagement;

public class MainMenu : NetworkBehaviour
{
    // Main Menu list
    public GameObject mainMenu;
    public List<GameObject> menus;

    // Menu refs
    public GameObject configureGraph;
    public GameObject waitingForClient;

    // For gameplay logic
    Config.Versus versus;
    
    // New Graph UI
    public TMP_InputField graphSizeInput;
    public TMP_InputField shortcutNumberInput;
    public TMP_InputField pointNodesInput;

    // Values for online
    public TMP_InputField hostIp;
    public TMP_InputField hostPort;


    public void Start()
    {
        NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
    }

    public void StartGame()
    {
        // Set values to config
        Config.Instance.versus = versus;
        if (graphSizeInput.text.Length > 0)
            Config.Instance.graphSize = int.Parse(graphSizeInput.text);
        else
            Config.Instance.graphSize = 4;
        if (shortcutNumberInput.text.Length > 0)
            Config.Instance.shortcuts = int.Parse(shortcutNumberInput.text);
        else
            Config.Instance.shortcuts = 3;
        if (pointNodesInput.text.Length > 0)
            Config.Instance.pointNodes = int.Parse(pointNodesInput.text);
        else
            Config.Instance.pointNodes = 3;


        // if client existsclient to change scene
        Debug.Log("Is server: " + NetworkManager.Singleton.IsServer);
        Debug.Log("Is host: " + NetworkManager.Singleton.IsHost);
        Debug.Log("Is client: " + NetworkManager.Singleton.IsClient);

        switch (Config.Instance.versus)
        {
            case Config.Versus.AI:
            case Config.Versus.PLAYER_PC:
                GameManager.Instance.StartGame();
                mainMenu.SetActive(false);
                break;
            case Config.Versus.PLAYER_ONLINE:
                if (NetworkManager.Singleton.IsHost)
                {
                    StartGameClientRpc();
                    GameManager.Instance.StartGame();
                    mainMenu.SetActive(false);
                }
                break;
            default:
                Debug.Assert(false, "Unreachable \'versus\' mode.");
                break;
        }
    }


    // GENERAL
    public void SwitchMenu(string name)
    {
        foreach (GameObject menu in menus)
        {
            if (menu.name == name)
                menu.SetActive(true);
            else
                menu.SetActive(false);
        }
    }
    public void SetVersus(int versus)
    {
        this.versus = (Config.Versus)versus;
    }

    // ONLINE RELATED
    public void StartHost()
    {
        NetworkManager.Singleton.StartHost();       
    }
    public void StartClient()
    {
        if (hostIp.text.Length > 0)
            NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectAddress = hostIp.text;
        if (hostPort.text.Length > 0)
            NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectPort = int.Parse(hostPort.text);

        NetworkManager.Singleton.StartClient();
    }
    public void StartServer()
    {
        NetworkManager.Singleton.StartServer();
    }

    [ClientRpc]
    private void StartGameClientRpc()
    {
        if (NetworkManager.Singleton.IsHost == false)
        {
            Config.Instance.versus = Config.Versus.PLAYER_ONLINE;
            mainMenu.SetActive(false);
        }
    }
    private void OnClientConnected(ulong id)
    {
        if (NetworkManager.Singleton.LocalClientId != id)
        {
            Debug.Log("Client Connected");
            waitingForClient.SetActive(false);
            configureGraph.SetActive(true);
        }
    }

}
