using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Node
{
    // Properties
    public int points;
    public Color color;
    public Vector3Int position;
    public List<Vector3Int> neighbors;
    public List<SimpleCurve> edges;
    public GameObject self;
    // Components
    private Animator animator;

    public enum Types
    {
        Simple,
        Point,
        Null
    }

    public Node()
    {
        neighbors = new List<Vector3Int>();
        edges = new List<SimpleCurve>();
        color = Color.white;
        points = 1;
    }
    public Node(Node node)
    {
        neighbors = new List<Vector3Int>(node.neighbors);
        edges = new List<SimpleCurve>();
        points = node.points;
        color = node.color;
        position = node.position;
        self = node.self;
    }

    public virtual void Init(Vector3Int position)
    {
        this.position = position;
    }

    public virtual void OnMouseDown()
    {
        // On Mouse down can be called only by clicking == local instance
        if (GameManager.Instance.IsLocalInstanceTurn() == false)
            return;
        Player player = GameManager.Instance.GetCurrentPlayer();
        if (CanBeClaimedBy(player) == false)
            return;

        GetClaimed(player);
        AudioManager.Instance.Play("nodeClaim");
        GameManager.Instance.CommitMove(position);
        GameManager.Instance.NextPlayer();
    }
    public bool CanBeClaimedBy(Player player)
    {
        if (    
                HasNeighborOfColor(player.color) == false &&
                GameManager.Instance.GetRoundNumber() > 0 ||
                color != Color.white
            )
            return false;
        else
            return true;
    }

    public void GetClaimed(Player player)
    {
        if (color != Color.white)
            return;

        player.nodes.Add(this);
        color = player.color;
        player.score += this.points;
        if (GameManager.Instance.isAI == false)
        {
            self.GetComponent<Renderer>().material = MaterialHolder.Instance.materials[color];
            foreach (SimpleCurve edge in edges)
                edge.SetNodeColor(self.gameObject, color);
        }
    }
    public void GetClaimedOptimized(Player player)
    {
        player.nodes.Add(this);
        color = player.color;
        player.score += this.points;
    }

    public bool HasNeighborOfColor(Color color)
    {
        bool flag = false;
        foreach (Vector3Int neighbor in neighbors)
        {
            if (Board.nodes[neighbor].color == color)
            {
                flag = true;
                break;
            }
        }
        return flag;
    }
}

// Point node will add more points and change the appearance of the node
public class PointNode : Node
{
    public PointNode(int points) : base()
    {
        neighbors = new List<Vector3Int>();
        edges = new List<SimpleCurve>();
        color = Color.white;
        this.points = points;
    }
    public PointNode(Node node) : base(node)
    {}

    public override void Init(Vector3Int position)
    {
        base.Init(position);
        self.GetComponent<Renderer>().material.color = new Color(177f/255f, 164f/255f, 40f/255f); // Dark yellow

        // Generate text above node;
        GameObject canvasInstance = Resources.Load<GameObject>("Prefabs/PointNode Canvas");
        GameObject canvas = MonoBehaviour.Instantiate(canvasInstance, this.self.transform);
        canvas.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = points + "p";
    }
    public override void OnMouseDown()
    {
        base.OnMouseDown();
    }
}