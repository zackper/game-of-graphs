using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;
    private AudioSource audioSource;
    [SerializeField]
    private List<string> audioNames;
    [SerializeField]
    private List<AudioClip> audioClips;


    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Play(string audioName, float delay = 0)
    {
        for (int i = 0; i < audioNames.Count; i++)
            if (audioName == audioNames[i])
            {
                audioSource.clip = audioClips[i];
                audioSource.PlayDelayed(delay);
            }
    }


    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Singleton Pattern
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    public static AudioManager Instance
    {
        get
        {
            return _instance;
        }
    }
}
