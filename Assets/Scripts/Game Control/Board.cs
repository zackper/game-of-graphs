using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using Unity.Netcode;

public struct Graph : INetworkSerializable
{
    public int sizeX;
    public int sizeY;

    public List<Vector2Int> edges;
    public List<Vector2Int> shortcuts;
    public List<int> pointNodes;
    public List<int> pointValues;
    public List<int> nullNodes;

    // INetworkSerializable
    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref sizeX);
        serializer.SerializeValue(ref sizeY);

        if (serializer.IsReader)
        {
            edges = new List<Vector2Int>();
            shortcuts = new List<Vector2Int>();
            pointNodes = new List<int>();
            pointValues = new List<int>();
            nullNodes = new List<int>();
        }

        SerializeVector2IntList(serializer, edges);
        SerializeVector2IntList(serializer, shortcuts);
        SerializeIntList(serializer, pointNodes);
        SerializeIntList(serializer, pointValues);
        SerializeIntList(serializer, nullNodes);
    }

    public void SerializeVector2IntList<A>(BufferSerializer<A> serializer, List<Vector2Int> list) where A : IReaderWriter
    {
        int length = 0;
        if (serializer.IsWriter)
            length = list.Count;
        serializer.SerializeValue(ref length);
        
        for(int i = 0; i < length; i++)
        {
            Vector2Int value = Vector2Int.zero;
            if (serializer.IsWriter)
                value = list[i];

            serializer.SerializeValue(ref value);

            if (serializer.IsReader)
                list.Add(value);
        }
    }
    public void SerializeIntList<A>(BufferSerializer<A> serializer, List<int> list) where A : IReaderWriter
    {
        int length = 0;
        if (serializer.IsWriter)
            length = list.Count;
        serializer.SerializeValue(ref length);

        for (int i = 0; i < length; i++)
        {
            int value = -1;
            if (serializer.IsWriter)
                value = list[i];

            serializer.SerializeValue(ref value);

            if (serializer.IsReader)
                list.Add(value);
        }
    }

    public Graph(int sizeX, int sizeY)
    {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        edges = new List<Vector2Int>();
        shortcuts = new List<Vector2Int>();
        pointNodes = new List<int>();
        pointValues = new List<int>();
        nullNodes = new List<int>();
    }

    static public Graph WriteNewGraph(int graphSize = 6, int shortcutEdgesNo = 2, int pointNodesNo = 2)
    {
        Graph graph = new Graph(graphSize, graphSize);
        // Set Size
        graph.sizeX = graphSize;
        graph.sizeY = graphSize;
        // Connect all neighbors
        for(int i = 0; i < graph.sizeX; i++)
        {
            for(int j = 0; j < graph.sizeY; j++)
            {
                for(int m = -1; m <= 1; m++)
                {
                    for(int n = -1; n <= 1; n++)
                    {
                        if (
                                i + m >= 0 && i + m < graph.sizeX &&
                                j + n >= 0 && j + n < graph.sizeY &&
                                !(m == 0 && n == 0) && !(Mathf.Abs(m) + Mathf.Abs(n) == 2)
                            )
                        {
                            int index1 = i * graph.sizeY + j;
                            int index2 = (i + m) * graph.sizeY + (j + n);
                            graph.edges.Add(new Vector2Int(index1, index2));
                        }
                    }
                }
            }
        }
        // Create shortcut nodes
        for (int i = 0; i < shortcutEdgesNo; i++)
        {
            int index1 = Random.Range(0, graph.sizeX * graph.sizeY - 1);
            int index2 = index1;
            while(index2 == index1)
                index2 = Random.Range(0, graph.sizeX * graph.sizeY - 1);

            graph.shortcuts.Add(new Vector2Int(index1, index2));
        }
        // Create point nodes
        for(int i = 0; i < pointNodesNo; i++)
        {
            graph.pointNodes.Add(Random.Range(0, graph.sizeX * graph.sizeY));
            graph.pointValues.Add(Random.Range(2, 5));
        }

        return graph;
    }
}

public class Board : NetworkBehaviour
{
    // Board Related
    public Transform boardOrigin;
    public float tileSize = 0.41f;
    public int boardSize = 5;
    public float magnetStr = 12f;

    // Number of smallWorld nodes
    private Graph graph;
    public int graphSize = 6;
    public int shortcutEdgesNo = 2;
    public int pointNodesNo = 2;

    private int readyPlayers = 0;

    // Prefabs
    [SerializeField]
    private GameObject nodePrefab = null;

    // Hash map for board objects
    public static Dictionary<Vector3Int, Node> nodes;

    // Start is called before the first frame update
    void Start()
    {}
    
    public void CreateBoard()
    {
        nodes = new Dictionary<Vector3Int, Node>();
        graph = Graph.WriteNewGraph(Config.Instance.graphSize, Config.Instance.shortcuts, Config.Instance.pointNodes);
        GenerateBoard();
    }
    public void SetBoard(Graph graph)
    {
        nodes = new Dictionary<Vector3Int, Node>();
        this.graph = graph;
        GenerateBoard();
    }

    public void DestroyGraph()
    {
        if (nodes != null)
        {
            List<Vector3Int> keys = new List<Vector3Int>(nodes.Keys);
            for (int i = 0; i < keys.Count; i++)
            {
                Destroy(nodes[keys[i]].self);
            }
        }
        
    }

    // In the future this may be "parse" graph array | DEPRICATED
    private void ReadGraph()
    {
        TextAsset graphString = Resources.Load<TextAsset>("Python/gog");
        JSONNode jsonGraph = JSON.Parse(graphString.text) as JSONNode;

        graph = new Graph();
        graph.sizeX = jsonGraph["sizeX"].AsInt;
        graph.sizeY = jsonGraph["sizeY"].AsInt;

        // Retrieve edges
        JSONArray edges = jsonGraph["edges"].AsArray;
        for(int i = 0; i < edges.Count; i++)
        {
            graph.edges.Add(new Vector2Int(edges[i][0].AsInt, edges[i][1].AsInt));
        }
        // Retrieve shortcut edges
        JSONArray shortcuts = jsonGraph["shortcuts"].AsArray;
        for (int i = 0; i < shortcuts.Count; i++)
        {
            graph.shortcuts.Add(new Vector2Int(shortcuts[i][0].AsInt, shortcuts[i][1].AsInt));
        }
        // Retrieve point nodes
        JSONArray pointNodes = jsonGraph["pointNodes"].AsArray;
        JSONArray pointValues = jsonGraph["pointValues"].AsArray;
        for (int i = 0; i < pointNodes.Count; i++)
        {
            graph.pointNodes.Add(pointNodes[i].AsInt);
            graph.pointValues.Add(pointValues[i].AsInt);
        }
        // Retrieve null nodes
        JSONArray nullNodes = jsonGraph["nullNodes"].AsArray;
        for (int i = 0; i < nullNodes.Count; i++)
        {
            graph.nullNodes.Add(nullNodes[i].AsInt);
        }
    }

    private void GenerateBoard()
    {
        tileSize = (float)boardSize / (float)graph.sizeX; // This assumes graph is a rectangle.
        boardOrigin.localPosition = new Vector3(-(float)boardSize / (float)2, 0, -(float)boardSize / (float)2) + new Vector3(tileSize / 2, 0.1f, tileSize / 2); ;

        // It is important to first add the special nodes, and later on add the simple nodes.
        // Instantiate all the special node gameobjects
        for (int i = 0; i < graph.pointNodes.Count; i++)
        {
            Vector3Int pos = new Vector3Int(graph.pointNodes[i] % graph.sizeX, 0, graph.pointNodes[i] / graph.sizeX);
            if(nodes.ContainsKey(pos) == false)
                CreateNode(pos, Node.Types.Point, graph.pointValues[i]);
        }
        // Instantiate all the simple node gameobjects
        for (int i = 0; i < graph.sizeX; i++)
        {
            for (int j = 0; j < graph.sizeY; j++)
            {
                Vector3Int pos = new Vector3Int(i, 0, j);
                if (nodes.ContainsKey(pos) == false)
                    CreateNode(pos, Node.Types.Simple);
            }
        }
        // Connect all the edges
        for(int i = 0; i < graph.edges.Count; i++)
        {
            int index1 = graph.edges[i].x;
            int index2 = graph.edges[i].y;
            Vector3Int pos1 = new Vector3Int(index1 % graph.sizeX, 0, index1 / graph.sizeX);
            Vector3Int pos2 = new Vector3Int(index2 % graph.sizeX, 0, index2 / graph.sizeX);
            ConnectNodes(nodes[pos1], nodes[pos2]);
        }
        // Connect all shortcuts
        for (int i = 0; i < graph.shortcuts.Count; i++)
        {
            int index1 = graph.shortcuts[i].x;
            int index2 = graph.shortcuts[i].y;
            Vector3Int pos1 = new Vector3Int(index1 % graph.sizeX, 0, index1 / graph.sizeX);
            Vector3Int pos2 = new Vector3Int(index2 % graph.sizeX, 0, index2 / graph.sizeX);
            ConnectNodes(nodes[pos1], nodes[pos2], true);
        }
    }

    public void CreateNode(Vector3Int pos, Node.Types type, int points = 1)
    {
        GameObject nodeObj = Instantiate(nodePrefab, boardOrigin);
        nodeObj.transform.localScale *= tileSize * 2/3;
        nodeObj.transform.localPosition = (Vector3)pos * tileSize;

        Node node = null;
        switch (type)
        {
            case Node.Types.Simple:
                nodeObj.GetComponent<NodeGameObject>().node = new Node();
                break;
            case Node.Types.Point:
                nodeObj.GetComponent<NodeGameObject>().node = new PointNode(points);
                break;
            default:
                Debug.Assert(false, "Unknown type of node");

                break;
        }
        node = nodeObj.GetComponent<NodeGameObject>().node;
        node.self = nodeObj;
        node.Init(pos);
        nodes.Add(pos, node);
    }

    public void ConnectNodes(Node node1, Node node2, bool isCurved = false)
    {
        if (node1.neighbors.Contains(node2.position) == false)
        {
            node1.neighbors.Add(node2.position);
            node2.neighbors.Add(node1.position);

            SimpleCurve edge;
            if(isCurved == false)
                edge = EdgeFactory.Instance.CreateEdge(node1.self.transform, node2.self.transform);
            else
            {
                // Create curved edge between nodes
                Vector3 boardCenter = new Vector3(boardSize / 2, 0, boardSize / 2);
                Transform middle = new GameObject().transform;
                middle.SetParent(boardOrigin);
                middle.localPosition = (node1.self.transform.localPosition + node2.self.transform.localPosition) / 2;
                middle.localPosition += new Vector3(0, Random.Range(5f, 8f), 0);

                int xSign = ((middle.localPosition.x - boardCenter.x) > 0 ? 1 : -1);
                int ySign = ((middle.localPosition.z - boardCenter.z) > 0 ? 1 : -1);

                // Find position for middle point based on gravitational pull 
                float r = Vector3.Distance(middle.localPosition, boardCenter);
                float cosThita = (middle.localPosition.x - boardCenter.x) / r;
                for (int i = 0; i < 3; i++)
                    r = r + 2 * (magnetStr - r) / 3;

                float dx = cosThita * r;
                float dy = Mathf.Sqrt(r * r - dx * dx) * ySign;
                middle.localPosition += new Vector3(dx, 0, dy);
                
                edge = EdgeFactory.Instance.CreateEdge(node1.self.transform, middle, node2.self.transform, 12);
                //Destroy(middle.gameObject);
            }

            node1.edges.Add(edge);
            node2.edges.Add(edge);
        }
    }

    public Graph GetGraph()
    {
        return graph;
    }
}
