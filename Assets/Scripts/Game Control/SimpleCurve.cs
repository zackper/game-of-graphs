using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCurve : MonoBehaviour
{
    private LineRenderer lr;
    GameObject node1;
    GameObject node2;
    
    private void Awake()
    {
        lr = GetComponent<LineRenderer>();
        lr.startWidth = 0.1f;
        lr.endWidth = 0.1f;
    }

    public void MakeCurve(Transform start, Transform middle, Transform end, float vertexCount = 12)
    {
        node1 = start.gameObject;
        node2 = end.gameObject;

        var pointList = new List<Vector3>();
        for (float lerpStep = 0; lerpStep <= 1; lerpStep += 1 / vertexCount)
        {
            var tangent1 = Vector3.Lerp(start.position, middle.position, lerpStep);
            var tangent2 = Vector3.Lerp(middle.position, end.position, lerpStep);
            var curve = Vector3.Lerp(tangent1, tangent2, lerpStep);
            pointList.Add(curve);
        }
        lr.positionCount = pointList.Count;
        lr.SetPositions(pointList.ToArray());
    }

    public void SetNodeColor(GameObject node, Color color)
    {
        if (node1 == node)
            lr.startColor = color;
        else
            lr.endColor = color;
    }
}
