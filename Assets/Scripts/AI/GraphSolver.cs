using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class TotalDecisionNode
{
    // Variables
    public GameState gamestate;
    // Iterating variables
    public Vector3Int myDecision;
    // Next best move:
    public Vector3Int nextBestMove;
    public TotalDecisionNode parent = null;
    public Dictionary<Vector3Int, TotalDecisionNode> children = null;

    private float winChance = 0f;
    private bool isRedWinning = false;
    private bool hasGeneratedChildren = false;
    private bool hasGeneratedWinningChances = false;
    private bool hasGeneratedWinningPaths = false;

    /**
     * This constructor is only referened to the first node of the TotalDecisionTree.
     * No nodes should have been claimed in this point. A new game is about to start.
     */
    public TotalDecisionNode(GameState initialGamestate)
    {
        gamestate = initialGamestate.DeepClone();
        GraphSolver.CreateDogma(gamestate.hash, this);
        children = new Dictionary<Vector3Int, TotalDecisionNode>();
    }
    /**
     * Takes as parameter parents gamestate and new decision.
     * Deep copies and updates gamestate based on myDecision.
     * The user shouldnt probably be able to use this constructor.
     */
    private TotalDecisionNode(GameState parentsGamestate, Vector3Int myDecision)
    {
        // Clone gamestate
        gamestate = parentsGamestate.DeepClone();
        // Initialize variables;
        children = new Dictionary<Vector3Int, TotalDecisionNode>();
        // Set my decision
        this.myDecision = myDecision;
        // Update myDecision to playing player.
        Player player = gamestate.GetCurrentPlayer();
        gamestate.nodes[myDecision].GetClaimedOptimized(player);
        gamestate.UpdateHash(myDecision, player.color);
        gamestate.NextPlayer();
    }

    /**
     * Generate children TDNode based on current player's available choices
     */
    public void GenerateChildren(int depth = -1)
    {
        if (hasGeneratedChildren == true)
            return;

        // Step 1: Find available for current player.
        Player player = gamestate.GetCurrentPlayer();

        if (gamestate.roundNumber > depth && depth > 0)
        {
            // Be the last layer.
        }
        else if (player.nodes.Count == 0) // First round. Generate one TDNode for each white node
        {
            List<Node> nodes = new List<Node>(gamestate.nodes.Values);
            foreach (Node node in nodes)
            {
                if (node.color == Color.white && children.ContainsKey(node.position) == false)
                {
                    TotalDecisionNode child = new TotalDecisionNode(gamestate, node.position);
                    children.Add(node.position, child);
                    GraphSolver.CreateDogma(child.gamestate.hash, child);
                }
            }
        }
        else if (player.nodes.Count > 0) // if first decision has already been made
        {
            foreach (Node node in player.nodes)
            {
                foreach (Vector3Int neighborPos in node.neighbors)
                {
                    Node neighbor = gamestate.nodes[neighborPos];
                    if (neighbor.color == Color.white && children.ContainsKey(neighbor.position) == false)
                    {
                        TotalDecisionNode child;
                        child = new TotalDecisionNode(gamestate, neighbor.position);
                        if (GraphSolver.InDogma(child.gamestate.hash) == false)
                        {
                            GraphSolver.CreateDogma(child.gamestate.hash, child);
                        }
                        else
                        {
                            GraphSolver.existingDogmaCase++;
                            child = GraphSolver.GetDogma(child.gamestate.hash);
                        }
                        children.Add(neighbor.position, child);
                    }
                }
            }
        }

        if (children.Count == 0)
        {
            if (gamestate.GetWinner().color == Color.red)
                isRedWinning = true;
        }
        hasGeneratedChildren = true;
        List<TotalDecisionNode> childrenList = new List<TotalDecisionNode>(children.Values);
        foreach (TotalDecisionNode child in childrenList)
        {
            child.GenerateChildren();
        }
    }

    /**
     * By the end of this function, all the nodes will have a winChance
     * assigned to them.
     * WinChance = children's.winChance / children.Count
     */
    public float FindWinChances()
    {
        if (hasGeneratedWinningChances)
            return winChance;

        if (children.Count == 0)
        {
            if (gamestate.players[0].score > gamestate.players[1].score)
                winChance = 1f;
            else if (gamestate.players[0].score == gamestate.players[1].score)
                winChance = 0.5f;
            else
                winChance = 0f;
        }
        else
        {
            List<TotalDecisionNode> childrenNodes = new List<TotalDecisionNode>(children.Values);
            
            for(int i = 0; i < childrenNodes.Count; i++)
            {
                TotalDecisionNode node = childrenNodes[i];
                float childWinChance = node.FindWinChances();
                this.winChance += childWinChance;
            }
            this.winChance /= childrenNodes.Count;
            if (GetPlayerOfLayer().color == Color.blue)
                this.winChance = 1 - this.winChance;
        }

        hasGeneratedWinningChances = true;
        return winChance;
    }

    /**
     * Tries to find a dominating winning path for red red player.
     * For Blue layers: Red needs atleast one winning child.
     * For Red layers: Red needs all children to be winning.
     */
    public void FindWinningPaths()
    {
        if (hasGeneratedWinningPaths)
            return;

        if (children.Count == 0) // I am leaf
        {
            if (gamestate.players[0].score == gamestate.players[1].score)
                isRedWinning = false;
            else if (gamestate.GetWinner().color == Color.red)
                isRedWinning = true;
            else
                isRedWinning = false;
        }
        else
        {
            Player playerOfLayer = GetPlayerOfLayer();
            List<TotalDecisionNode> childrenList = new List<TotalDecisionNode>(children.Values);
            if(playerOfLayer.color == Color.blue) // At least one child must be winning for red to win
            {
                foreach (var child in childrenList)
                {
                    child.FindWinningPaths();
                    if (child.isRedWinning == true)
                        isRedWinning = true;
                }
            }
            else if(playerOfLayer.color == Color.red) // All children must be winning for red to win
            {
                bool areAllChildrenWinnning = true;
                foreach (var child in childrenList)
                {
                    child.FindWinningPaths();
                    if (child.isRedWinning == false)
                        areAllChildrenWinnning = false;
                }
                isRedWinning = areAllChildrenWinnning;
            }
        }
        hasGeneratedWinningPaths = true;
    }

    /**
     * Returns the best move for current player.
     * If no winning path exists, select the one that has the maximum winchance
     */
    public Vector3Int GetBestMove()
    {
        Debug.Assert(hasGeneratedChildren == true, "Trying to get wining move, but you havent calculated TDTree children");
        Player currentPlayer = GameManager.Instance.GetCurrentPlayer();

        float highestWinningChance = -1f;
        Vector3Int bestMove = new Vector3Int(-1, -1, -1);
        List<Vector3Int> keys = new List<Vector3Int>(children.Keys);

        // No matter what player you are, pick the highest winning option
        // Unless you are the red player and you find a winning node
        foreach (var key in keys)
        {
            var child = children[key];
            if(child.winChance > highestWinningChance)
            {
                bestMove = key;
                highestWinningChance = child.winChance;
            }
            if(child.isRedWinning && currentPlayer.color == Color.red)
            {
                Debug.Log("Absolute win move");
                bestMove = key;
                break;
            }
        }

        Debug.Log("Best Move: " + bestMove);
        Debug.Log("Win Chance: " + highestWinningChance);
        return bestMove;
    }

    public Player GetPlayerOfLayer()
    {
        return gamestate.players[(gamestate.roundNumber + 1) % gamestate.players.Count];
    }
    public Player GetEnemyPlayerOfLayer()
    {
        return gamestate.players[(gamestate.roundNumber) % gamestate.players.Count];
    }
}

public class GraphSolver
{
    GameState   gamestate;
    public int  graphSize;

    public static int existingDogmaCase;
    public static Dictionary<string, TotalDecisionNode> dogma;

    public GraphSolver(GameState gamestate, int graphSize)
    {
        this.gamestate = gamestate;
        this.graphSize = graphSize;

        existingDogmaCase = 0;
        dogma = new Dictionary<string, TotalDecisionNode>();
    }

    public void FindTotalDecisionTree()
    {
        TotalDecisionNode tree = new TotalDecisionNode(gamestate);
        tree.GenerateChildren();
        tree.FindWinningPaths();
        tree.FindWinChances();
        Debug.Log("Total Dogmas in TDTree: " + GraphSolver.dogma.Count);
        Debug.Log("Total skiped branch recalculations: " + GraphSolver.existingDogmaCase);
    }
    public void PlayWinningMove(GameState gamestate)
    {
        gamestate.GenerateHash();
        Debug.Log("~~~~~~");
        Debug.Log("Searching best move for: " + new string(gamestate.hash));
        Vector3Int winningMove = GetDogma(gamestate.hash).GetBestMove();
        gamestate.nodes[winningMove].OnMouseDown();
    }

    public static void              CreateDogma(char[] hash, TotalDecisionNode node)
    {
        dogma.Add(new string(hash), node);
    }
    public static bool              InDogma(char[] hash)
    {
        return GraphSolver.dogma.ContainsKey(new string(hash));
    }
    public static TotalDecisionNode GetDogma(char[] hash)
    {
        return GraphSolver.dogma[new string(hash)];
    }
}
