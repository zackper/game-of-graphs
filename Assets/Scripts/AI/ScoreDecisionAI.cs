﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class ScoreDecisionAI
{
    private GameState   gamestate;
    private Player      self;
    private Dictionary<Vector3Int, int> scoreMap;

    public ScoreDecisionAI(GameState gamestate, Player self)
    {
        this.gamestate = gamestate;
        this.self = self;

        // Initialize scoreMap with everything being 0
        scoreMap = new Dictionary<Vector3Int, int>();
        foreach (KeyValuePair<Vector3Int, Node> pair in gamestate.nodes)
        {
            scoreMap.Add(pair.Key, 0);
        }
    }

    public Vector3Int FindBestMove()
    {
        Vector3Int  bestMove = new Vector3Int(-1, -1, -1);
        int         bestScore = 0;

        if (gamestate.GetRoundNumber() > 0)
        {
            var bfsScore = FindAllBfsScore();

            foreach (KeyValuePair<Vector3Int, int> pair in bfsScore)
                if (pair.Value > bestScore)
                {
                    bestMove = pair.Key;
                    bestScore = pair.Value;
                }
        }
        else
        {
            // Round == 0, so I have no nodes. Check all nodes and find the highest score.
            foreach(KeyValuePair<Vector3Int, Node> pair in gamestate.nodes)
            {
                // Score based: isPoint + hasShortcuts + center distance
                Node node = pair.Value;
                if (node.color != Color.white)
                    continue;

                int nodeScore = node.points + node.neighbors.Count;
                foreach (Vector3Int neighborPos in node.neighbors)
                    nodeScore += gamestate.nodes[neighborPos].points / 2;

                if(nodeScore > bestScore)
                {
                    bestScore = nodeScore;
                    bestMove = pair.Key;
                }
            }
        }

        Debug.Assert(bestMove != new Vector3(-1, -1, -1), "No move could be found");

        return bestMove;
    }

    private Dictionary<Vector3Int, int> FindAllBfsScore()
    {
        Dictionary<Vector3Int, int> bfsScore = new Dictionary<Vector3Int, int>();

        Player currentPlayer = gamestate.GetCurrentPlayer();
        foreach(Node node in currentPlayer.nodes)
            foreach(Vector3Int neighborPos in node.neighbors)
            {
                Node neighbor = gamestate.nodes[neighborPos];
                if (neighbor.color == Color.white && bfsScore.ContainsKey(neighborPos) == false)
                {
                    // Create clone of state and play the move
                    GameState clone = this.gamestate.DeepClone();
                    clone.nodes[neighborPos].GetClaimedOptimized(clone.GetCurrentPlayer());
                    // Find bfs score in that gamestate
                    int[] scores = FindBfsScore(clone);
                    bfsScore.Add(neighborPos, scores[currentPlayer.id]);
                }
            }

        return bfsScore;
    }
    private int[] FindBfsScore(GameState gamestate)
    {
        List<Vector3Int> neutralNodes = new List<Vector3Int>();
        List<List<Vector3Int>> currentClaims = new List<List<Vector3Int>>();

        currentClaims.Add(new List<Vector3Int>());
        currentClaims.Add(new List<Vector3Int>());

        do
        {
            // Step 0: Empty claim lists
            currentClaims[0].Clear();
            currentClaims[1].Clear();
            // Step 1: for each player mark current claims.
            for (int i = 0; i < gamestate.players.Count; i++)
            {
                Player p = gamestate.players[i];
                foreach (Node node in p.nodes)
                    foreach (Vector3Int neighborPos in node.neighbors)
                    {
                        Node neighbor = gamestate.nodes[neighborPos];
                        if (neighbor.color == Color.white && neutralNodes.Contains(neighborPos) == false)
                        {
                            currentClaims[i].Add(neighborPos);
                        }
                    }
            }
            // Step 2: Mark neutral and remove the intersection of claims
            for (int j = 0; j < currentClaims[0].Count; j++) // Mark neutral
            {
                Vector3Int pos = currentClaims[0][j];
                if (currentClaims[1].Contains(pos))
                {
                    neutralNodes.Add(pos);
                }
            }
            foreach (Vector3Int pos in neutralNodes) // Remove from lists
            {
                currentClaims[0].Remove(pos);
                currentClaims[1].Remove(pos);
            }
            // Step 3: Claim remaining nodes for each player
            for (int i = 0; i < gamestate.players.Count; i++)
            {
                foreach (Vector3Int claim in currentClaims[i])
                    gamestate.nodes[claim].GetClaimedOptimized(gamestate.players[i]);
            }
            gamestate.CheckTerritoryClaims(false);
        } while (currentClaims[0].Count > 0 || currentClaims[1].Count > 0); // If there are still nodes to claim.

        // Count score and return
        int[] scores = new int[2];
        for (int i = 0; i < gamestate.players.Count; i++)
        {
            scores[i] = 0;
            Player p = gamestate.players[i];
            foreach(Node node in p.nodes)
            {
                scores[i] += node.points;
            }
        }

        return scores;
    }
    
    public Player GetSelf()
    {
        return self;
    }
};
