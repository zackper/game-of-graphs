using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeGameObject : MonoBehaviour
{
    public Node node;

    private void Awake()
    {
    }

    public void Init(Vector3Int pos)
    {
        node.Init(pos);
    }
    private void OnMouseDown()
    {
        node.OnMouseDown();
    }
}
