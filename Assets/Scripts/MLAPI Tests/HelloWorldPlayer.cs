using Unity.Netcode;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HelloWorld
{
    public struct StructPosition : INetworkSerializable
    {
        public int id;
        public List<Vector3> positions;

        // INetworkSerializable
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref id);
            
            int length = 0;
            if (serializer.IsWriter)
                length = positions.Count;

            serializer.SerializeValue(ref length);

            if (serializer.IsReader)
                positions = new List<Vector3>();
            
            for (int i = 0; i < length; i++)
            {
                Vector3 value = Vector3.negativeInfinity;

                if (serializer.IsWriter)
                    value = positions[i];

                serializer.SerializeValue(ref value);

                if (serializer.IsReader)
                    positions.Add(value);
            }
        }
    }


    public class HelloWorldPlayer : NetworkBehaviour
    {
        public NetworkVariable<Vector3> Position = new NetworkVariable<Vector3>();

        public override void OnNetworkSpawn()
        {
            if (IsOwner)
            {
                Move();
            }
        }

        public void Move()
        {
            if (NetworkManager.Singleton.IsServer)
            {
                var randomPosition = GetRandomPositionOnPlane();
                transform.position = randomPosition;
                Position.Value = randomPosition;
            }
            else
            {
                SubmitPositionRequestServerRpc();
            }
        }

        [ServerRpc]
        void SubmitPositionRequestServerRpc(ServerRpcParams rpcParams = default)
        {
            Position.Value = GetRandomPositionOnPlane();
        }        

        static Vector3 GetRandomPositionOnPlane()
        {
            return new Vector3(Random.Range(-3f, 3f), 1f, Random.Range(-3f, 3f));
        }


        public void ForceMoveClient()
        {
            Debug.Log("Forcing new position to client");
            StructPosition newClientPos = new StructPosition();
            newClientPos.id = 1;
            newClientPos.positions = new List<Vector3>();
            newClientPos.positions.Add(new Vector3(0, 0, 0));
            ForceMoveClientRpc(newClientPos);
        }

        [ClientRpc]
        void ForceMoveClientRpc(StructPosition test)
        {
            Debug.Log("Client RPC Called by server");
            Debug.Log("Test: " + test);
            Debug.Log("Test.id: " + test.id);
            Debug.Log("Test.id: " + test.positions[0]);
            transform.position = test.positions[0];
        }


        void Update()
        {
            //transform.position = Position.Value;
        }
    }
}