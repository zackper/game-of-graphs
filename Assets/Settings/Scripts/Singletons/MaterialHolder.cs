﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class MaterialHolder
{
    private static readonly MaterialHolder instance = new MaterialHolder();
    public Dictionary<Color, Material> materials;

    private MaterialHolder()
    {
        materials = new Dictionary<Color, Material>();

        // Load Materials from Resources folder
        Material white = Resources.Load<Material>("Node Materials/Default_Node_Mat");
        Material red = Resources.Load<Material>("Node Materials/Red_Node_Mat");
        Material blue = Resources.Load<Material>("Node Materials/Blue_Node_Mat");
        Debug.Assert(white && red && blue, "MaterialHolder not intialized correctly!");
        // Add them to materials Dictionary
        materials.Add(Color.white, white);
        materials.Add(Color.red, red);
        materials.Add(Color.blue, blue);
    }

    // Explicit static constructor to tell C# compiler
    // not to mark type as beforefieldinit
    static MaterialHolder(){}
    public static MaterialHolder Instance
    {
        get
        {
            return instance;
        }
    }
}