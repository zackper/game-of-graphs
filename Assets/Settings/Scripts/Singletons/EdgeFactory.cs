using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class EdgeFactory
{
    private static readonly EdgeFactory instance = new EdgeFactory();
    private GameObject  simpleCurvePrefab;
    private GameObject  factoryObject;
    private Material    edgeMaterial;

    private EdgeFactory()
    {
        //Resources.Load<GameObject>("Prefabs/SimpleCurve");
        factoryObject = new GameObject("Edge Factory");
        edgeMaterial = Resources.Load<Material>("Line Materials/Line Material");
    }

    // This is called to create curved edges
    public SimpleCurve CreateEdge(Transform p1, Transform p2, Transform p3, float vertexCount)
    {
        GameObject simpleCurveObj = new GameObject();
        simpleCurveObj.transform.SetParent(factoryObject.transform);
        LineRenderer lr = simpleCurveObj.AddComponent<LineRenderer>();
        lr.material.color = Color.white;
        simpleCurveObj.GetComponent<Renderer>().material = edgeMaterial;
        simpleCurveObj.AddComponent<SimpleCurve>().MakeCurve(p1, p2, p3, vertexCount);
        return simpleCurveObj.GetComponent<SimpleCurve>();
    }
    // This is called to create straight edges
    public SimpleCurve CreateEdge(Transform p1, Transform p2)
    {
        Transform p3 = p2;
        return CreateEdge(p1, p2, p3, 3);
    }
    
    // Destroy all edges to reset game
    public void DestroyEdges()
    {
        for(int i = factoryObject.transform.childCount - 1; i >= 0; i--)
        {
            MonoBehaviour.Destroy(factoryObject.transform.GetChild(i).gameObject);
        }
    }

    // Explicit static constructor to tell C# compiler
    // not to mark type as beforefieldinit
    static EdgeFactory() { }
    public static EdgeFactory Instance
    {
        get
        {
            return instance;
        }
    }
}