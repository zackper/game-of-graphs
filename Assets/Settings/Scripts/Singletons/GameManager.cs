﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;


public class Player
{
    public int          id;
    public string       name;
    public int          score;
    public Color        color;
    public Color        uiColor;
    public List<Node>   nodes;

    public Player(int id, string name, Color color)
    {
        this.id = id;
        this.name = name;
        this.score = 0;
        this.color = color;
        this.uiColor = color;

        nodes = new List<Node>();
    }
    public Player(Player player)
    {
        this.id = player.id;
        this.name = player.name;
        this.score = player.score;
        this.color = player.color;
        this.uiColor = player.color;
        nodes = new List<Node>();
    }
}

public class GameState
{
    public char[] hash;
    public int graphSize;

    public int roundNumber;
    public bool isOver;

    public Dictionary<Vector3Int, Node> nodes;
    public List<Player> players;

    public GameState(Dictionary<Vector3Int, Node> nodes, List<Player> players, int roundNumber)
    {
        this.nodes = nodes;
        this.players = players;
        this.roundNumber = roundNumber;
        this.graphSize = (int)Mathf.Sqrt(nodes.Count);
    }

    public void EndGame()
    {
        isOver = true;
    }
    public Player GetWinner()
    {
        Player winner = players[0];
        foreach (Player player in players)
            if (player.score > winner.score)
                winner = player;

        return winner;
    }
    public bool isTie()
    {
        if (players[0].score == players[1].score)
            return true;
        else
            return false;
    }
    public int GetRoundNumber()
    {
        return roundNumber / 2;
    }
    public Player GetCurrentPlayer()
    {
        return players[roundNumber % players.Count];
    }
    public Player NextPlayer()
    {
        // Set current player's points
        Player currentPlayer = players[roundNumber % players.Count];
        // Change player and check if he has available moves
        Player nextPlayer = players[++roundNumber % players.Count];
        if (roundNumber + 1 - players.Count > 0) // Dont execute the first turn
        {
            CheckTerritoryClaims();
            if (HasAvailableMoves(nextPlayer) == false)
                EndGame();
        }

        return nextPlayer;
    }

    public bool HasAvailableMoves(Player player)
    {
        foreach (Node node in player.nodes)
        {
            foreach (Vector3Int neighbor in node.neighbors)
                if (nodes[neighbor].color == Color.white)
                    return true;
        }

        return false;
    }
    public void CheckTerritoryClaims(bool isRendered = true)
    {
        Dictionary<Vector3Int, Node> nodes = new Dictionary<Vector3Int, Node>();
        foreach (KeyValuePair<Vector3Int, Node> pair in this.nodes)
            if (pair.Value.color == Color.white)
                nodes.Add(pair.Key, pair.Value);

        while (nodes.Count > 0)
        {
            List<Vector3Int> keys = new List<Vector3Int>(nodes.Keys);
            Vector3Int key = keys[0];
            Color territory = Color.white;
            bool isClaimed = false;

            // BFS all neighbors. If they have the same color then claim territory
            List<Node> territoryList = new List<Node>();
            Stack<Vector3Int> stack = new Stack<Vector3Int>();
            stack.Push(key);
            isClaimed = true;
            while (stack.Count > 0)
            {
                Vector3Int bfsKey = stack.Pop();

                Node bfsNode = this.nodes[bfsKey]; // Take node from global nodes hashmap
                if (nodes.ContainsKey(bfsKey))
                    nodes.Remove(bfsKey); // Removal from (local)nodes works as marking it as "discovered"

                if (bfsNode.color == Color.white)
                {
                    territoryList.Add(bfsNode);
                    foreach (Vector3Int neighbor in bfsNode.neighbors)
                        if (this.nodes[neighbor].color != Color.white || nodes.ContainsKey(neighbor))
                            stack.Push(neighbor);
                }
                else // already claimed node
                {
                    if (territory == Color.white)
                        territory = bfsNode.color;

                    if (territory == bfsNode.color)
                        continue;
                    else if (territory != bfsNode.color)
                        isClaimed = false;
                }
            }
            if (isClaimed == true)
            {
                Player player = GetPlayerOfColor(territory);
                foreach (Node node in territoryList)
                {
                    UpdateHash(node.position, player.color);
                    if (isRendered == true)
                        node.GetClaimed(player);
                    else
                        node.GetClaimedOptimized(player);
                }
            }
        }
    }
    public Player GetPlayerOfColor(Color color)
    {
        foreach (Player player in players)
            if (player.color == color)
                return player;

        return null;
    }

    public char[] GenerateHash()
    {
        /**
         * Hash is maped based on indices.
         * 0 1 2 .... graphSize^2 - 1
         */
        // Hash will look like: "wwwwwwwwww"
        hash = new char[(graphSize * graphSize)];
        for (int i = 0; i < graphSize * graphSize; i++)
        {
            Node node = nodes[IndexToVector3(i)];
            hash[i] = ColorToChar(node.color);
        }
        return hash;
    }
    public void UpdateHash(Vector3Int decision, Color color)
    {
        hash[Vector3ToIndex(decision)] = ColorToChar(color);
    }

    public char ColorToChar(Color color)
    {
        if (color == Color.red)
            return 'r';
        else if (color == Color.blue)
            return 'b';

        return 'w';
    }
    public Color CharToColor(char c)
    {
        if (c == 'r')
            return Color.red;
        else if (c == 'b')
            return Color.blue;

        return Color.white;
    }
    public Vector3Int IndexToVector3(int index)
    {
        int i = index / graphSize;
        int j = index % graphSize;
        return new Vector3Int(i, 0, j);
    }
    public int Vector3ToIndex(Vector3Int vector)
    {
        return vector.x * graphSize + vector.z;
    }

    public GameState DeepClone()
    {
        List<Player> clonePlayers = new List<Player>();
        Dictionary<Vector3Int, Node> cloneNodes = new Dictionary<Vector3Int, Node>();

        // Iterate to fill dictionary of nodes
        List<Node> nodeList = new List<Node>(this.nodes.Values);
        foreach (Node node in nodeList)
        {
            Node copy = new Node(node);
            cloneNodes.Add(copy.position, copy);
        }

        foreach (Player player in this.players)
        {
            Player copy = new Player(player);
            foreach (Node node in player.nodes)
                copy.nodes.Add(cloneNodes[node.position]);

            clonePlayers.Add(copy);
        }

        GameState deepClone = new GameState(cloneNodes, clonePlayers, roundNumber);
        deepClone.hash = new char[graphSize * graphSize];
        for (int i = 0; i < graphSize * graphSize; i++)
            deepClone.hash[i] = hash[i];

        return deepClone;
    }
}

public class GameManager : NetworkBehaviour
{
    private static GameManager _instance;
    private GraphSolver     solver;
    private ScoreDecisionAI AI;
    private GameState       gamestate;
    private List<Player>    players;
    private int             roundNumber;
    private GameUI          gameUI;
    private Board           board;
    private Player          localInstancePlayer;

    // Not 100% sure why, but maybe remove later?!
    public bool             isAI = false;

    void Start()
    {
        board = GameObject.Find("Board").GetComponent<Board>();
        gameUI = GameObject.Find("Canvas").GetComponent<GameUI>();
    }

    public void ResetBoard()
    {
        // TODO: Deal with this later, too tired now
        // Reset graph
        if (board != null)
        {
            board.DestroyGraph();
            EdgeFactory.Instance.DestroyEdges();
        }
    }
    public void StartGame()
    {
        // Create playres
        players = new List<Player>();
        players.Add(new Player(0, "RED", Color.red));
        players.Add(new Player(1, "BLUE", Color.blue));

        if(Config.Instance.versus != Config.Versus.PLAYER_ONLINE)
        {
            ResetBoard();
            board.CreateBoard();
        }
        else if (IsHost) // If online, only host should create board
        {
            ResetBoard();
            board.CreateBoard();
            // Pass graph as parameter
            StartGameClientRpc(board.GetGraph());
        }

        // Create gamestate
        gamestate = new GameState(Board.nodes, players, roundNumber);
        gamestate.GenerateHash();

        //Decide and send which player is which instance. Ignore if you are client. Server will assign you
        int myInstanceId = Random.Range(0, players.Count);
        int opponentId = (players.Count - 1) - myInstanceId; // 1 - 1 -> 0 ||  1 - 0 -> 1
        if (Config.Instance.versus == Config.Versus.PLAYER_PC)
            localInstancePlayer = null;
        else if(Config.Instance.versus == Config.Versus.AI) // playing vs AI
        {
            localInstancePlayer = players[myInstanceId];
            AI = new ScoreDecisionAI(gamestate, players[opponentId]);
            if (opponentId == 0) // If AI players first then play.
                PlayAIMove();
        }
        else if(Config.Instance.versus == Config.Versus.PLAYER_ONLINE && IsHost)
        {
            localInstancePlayer = players[myInstanceId];
            SetLocalInstancePlayerClientRpc(opponentId);
        }

        gameUI.OnGameStart();
        // Reset UI
        gameUI.SetCurrentPlayer(GetCurrentPlayer().name, GetCurrentPlayer().uiColor);
        foreach (Player player in players)
            gameUI.SetPlayerPoints(player.score, player.color);

        solver = null;
        roundNumber = 0;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Online multiplayer required functions
    [ClientRpc]
    private void StartGameClientRpc(Graph graph)
    {
        if (IsHost == false)
        {
            gameUI.GetComponent<MainMenu>().SwitchMenu("none");
            ResetBoard();
            board.SetBoard(graph);
            StartGame();
        }
    }
    [ClientRpc]
    private void SetLocalInstancePlayerClientRpc(int id)
    {
        Debug.Log("Set local instance player: " + id);
        if (NetworkManager.Singleton.IsHost == false)
            localInstancePlayer = players[id];
        gameUI.SetCurrentPlayer(GetCurrentPlayer().name, GetCurrentPlayer().uiColor);
    }
    [ServerRpc(RequireOwnership = false)]
    private void CommitMoveServerRpc(Vector3Int move)
    {
        if (IsServer)
        {
            Debug.Log("Opponent commited");
            Board.nodes[move].GetClaimed(GetCurrentPlayer());
            NextPlayer();
        }
    }
    [ClientRpc]
    private void CommitMoveClientRpc(Vector3Int move)
    {
        if (IsHost == false)
        {
            Debug.Log("Opponent commited");
            Board.nodes[move].GetClaimed(GetCurrentPlayer());
            NextPlayer();
        }
    }
    public void CommitMove(Vector3Int move)
    {
        // If no network return ?!
        if (IsHost)
            CommitMoveClientRpc(move);
        else // is client
            CommitMoveServerRpc(move);
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // End game functions
    public void EndGame()
    {
        if (gamestate.isTie())
            gameUI.EndGameScreen("TIE", Color.white);
        else
        {
            Player winner = GetWinner();
            gameUI.EndGameScreen(winner.name, winner.color);
            if (localInstancePlayer == null || localInstancePlayer == winner)
                AudioManager.Instance.Play("victory", 0);
            else
                AudioManager.Instance.Play("defeat", 0);
        }
        solver = null;
    }
    public bool IsOver()
    {
        return gameUI.gameEnded.activeSelf;
    }
    public Player GetWinner()
    {
        return gamestate.GetWinner();
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Basic getters/setters
    public int GetRoundNumber()
    {
        return gamestate.GetRoundNumber();
    }
    public Player GetPlayerOfColor(Color color)
    {
        return gamestate.GetPlayerOfColor(color);
    }
    public Player GetCurrentPlayer()
    {
        return gamestate.GetCurrentPlayer();
    }
    public Player NextPlayer()
    {
        Player nextPlayer = gamestate.NextPlayer();
        gameUI.SetCurrentPlayer(nextPlayer.name, nextPlayer.uiColor);

        foreach (Player player in gamestate.players)
            gameUI.SetPlayerPoints(player.score, player.color);

        if (gamestate.isOver)
            EndGame();

        if (Config.Instance.versus == Config.Versus.AI && AI.GetSelf() == nextPlayer)
        {
            PlayAIMove();
        }

        return nextPlayer;
    }
    public bool IsLocalInstanceTurn()
    {
        // If local instance is null, then both players play from same pc.
        // If its not null, we have either remove or vs AI. Player is legit
        // if he is the current player
        if (localInstancePlayer == null || localInstancePlayer == GetCurrentPlayer())
            return true;
        else
            return false;
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Total Decision Tree functions
    public void PlayBestMove()
    {
        if(solver == null)
        {
            gameUI.StartCoroutine(GenerateSolver()); // This has to be a coroutine in order to enable ui first.
        }
        else
            solver.PlayWinningMove(gamestate);
    }
    public void GenerateTotalDecisionTree()
    {
        isAI = true;
        solver = new GraphSolver(gamestate, board.graphSize);
        solver.FindTotalDecisionTree();
        isAI = false;
    }
    public IEnumerator GenerateSolver()
    {
        //gameUI.aiLoadingScreen.SetActive(true);
        //yield return 0;
        //GenerateTotalDecisionTree();
        //gameUI.aiLoadingScreen.SetActive(false);
        //solver.PlayWinningMove(gamestate);
        yield return 1;
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // AI Based on Resources Scoring functions
    public void PlayAIMove()
    {
        Vector3Int bestMove = AI.FindBestMove();
        gamestate.nodes[bestMove].GetClaimed(AI.GetSelf());
        NextPlayer();
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Singleton Pattern
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }
}


public sealed class Config
{
    // For playing logic
    public enum Versus
    {
        AI,
        PLAYER_PC,
        PLAYER_ONLINE
    };
    public Versus versus;

    // For graph generation
    public int graphSize = 5;
    public int shortcuts = 4;
    public int pointNodes = 4;


    private Config()
    {
        
    }

    // Explicit static constructor to tell C# compiler
    // not to mark type as beforefieldinit
    private static readonly Config instance = new Config();
    static Config() { }
    public static Config Instance
    {
        get
        {
            return instance;
        }
    }
}